class Paper < ApplicationRecord
  belongs_to :issue
  has_one_attached :article
  validates :title, :author, :keywords, :category, presence: true
  validate :acceptable_article
  
  def acceptable_article
    return unless article.attached?

    unless article.blob.byte_size <= 1.megabyte
      errors.add(:article, "is too big")
    end

    acceptable_types = ["application/pdf"]
    unless acceptable_types.include?(article.content_type)
      errors.add(:article, "must be a PDF")
    end
  end
end
