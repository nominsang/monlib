class Document < ApplicationRecord
  has_many :items
  def display_name
    self.mongolian_title
  end
end
