class Item < ApplicationRecord
  belongs_to :document
  belongs_to :location
  has_many :volnos
end
