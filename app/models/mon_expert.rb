class MonExpert < ApplicationRecord
  validates :name, :occupation, :biographical_text, presence: true
  belongs_to :expert
end
