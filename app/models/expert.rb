class Expert < ApplicationRecord
  has_many :mon_experts
  has_one_attached :avatar
  validates :name, :occupation, :biographical_text, presence: true
end
