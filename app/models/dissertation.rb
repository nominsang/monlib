class Dissertation < ApplicationRecord
  validates :title, :author, :teacher, :keywords, :abstract, :student_id, :graduation_date, presence: true
  validates :student_id, :graduation_date, numericality: { only_integer: true }
  has_one_attached :thesis
  
  validate :acceptable_thesis
  
  def acceptable_thesis
    return unless thesis.attached?

    unless thesis.blob.byte_size <= 1.megabyte
      errors.add(:thesis, "太大了！")
    end

    acceptable_types = ["application/pdf"]
    unless acceptable_types.include?(thesis.content_type)
      errors.add(:thesis, "只能上传 PDF 文件！")
    end
  end
end
