ActiveAdmin.register Entity do
  belongs_to :volno
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :volno_id, :identify_number, :about
  #
  # or
  #
  # permit_params do
  #   permitted = [:volno_id, :identify_number, :about]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
