ActiveAdmin.register BookToc do
  belongs_to :book

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :book_id, :chapter, :title, :page, :file, :order
  #
  # or
  #
  # permit_params do
  #   permitted = [:book_id, :chapter, :title, :page, :file, :order]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
