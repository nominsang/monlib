ActiveAdmin.register MonExpert do
  belongs_to :expert

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :expert_id, 
                :name, 
                :alias_name, 
                :nation, 
                :birthplace, 
                :occupation, 
                :biographical_text
  #
  # or
  #
  # permit_params do
  #   permitted = [:expert_id, :name, :alias_name, :nation, :birthplace, :occupation, :biographical_text]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
