ActiveAdmin.register Dissertation do
  menu priority: 6
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  index do
    selectable_column
    id_column
    column :title
    column :author
    actions
  end
  
  form do |f|
    f.inputs do
      f.input :title
      f.input :author
      f.input :teacher
      f.input :keywords
      f.input :abstract
      f.input :student_id
      f.input :discipline
      f.input :research_field
      f.input :education
      f.input :degree
      f.input :graduation_date
      f.input :thesis, as: :file
    end
    actions
  end
  
  show do
    attributes_table do
      row :title
      row :author
      row :teacher
      row :keywords
      row :abstract
      row :student_id
      row :discipline
      row :research_field
      row :education
      row :degree
      row :graduation_date
      row :thesis do
        div do
          if dissertation.thesis.attached?
            dissertation.thesis.filename
          else
            "no"
          end
        end
      end
    end
  end
  
  permit_params :title, 
                :author, 
                :teacher, 
                :keywords, 
                :abstract, 
                :student_id, 
                :discipline, 
                :research_field, 
                :education, 
                :degree, 
                :graduation_date,
                :thesis
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :author, :teacher, :keywords, :abstract, :student_id, :discipline, :research_field, :education, :degree, :graduation_date]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
