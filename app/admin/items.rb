ActiveAdmin.register Item do
  belongs_to :document
  
  index do
    selectable_column
    id_column
    column :document
    column :location
    column :classification_code
    column :order_code
    column :volume
    column :copies
    actions
  end
  
  sidebar "卷册", only: [:show, :edit] do
    ul do
      li link_to "所有卷册", admin_item_volnos_path(resource)
      li link_to "新增卷册", new_admin_item_volno_path(resource)
    end
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :document_id, :location_id, :classification_code, :order_code, :volume, :copies, :about
  #
  # or
  #
  # permit_params do
  #   permitted = [:document_id, :location_id, :classification_code, :order_code, :volume, :copies, :about]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
