ActiveAdmin.register Expert do
  menu priority: 7
  sidebar "该学者对应蒙古文信息", only: [:show, :edit] do
    ul do
      li link_to "蒙古文信息", admin_expert_mon_experts_path(resource)
      unless resource.mon_experts.size >= 1
        li link_to "新增蒙古文信息", new_admin_expert_mon_expert_path(resource)
      end
    end
  end
  
  index do
    selectable_column
    id_column
    column :name
    column :sex
    column :nation
    column :occupation
    actions
  end
  
  show do
    attributes_table do
      row :name
      row :alias_name
      row :sex
      row :nation
      row :birthday
      row :deathday
      row :birthplace
      row :occupation
      row :biographical_text
      row :avatar do
        div do
          if expert.avatar.attached?
            expert.avatar.filename
          else
            "no"
          end
        end
      end
    end
  end
  
  form do |f|
    f.inputs do
      f.input :name
      f.input :alias_name
      f.input :sex
      f.input :nation
      f.input :birthday
      f.input :deathday
      f.input :birthplace
      f.input :occupation
      f.input :biographical_text
      f.input :avatar, as: :file    
    end
    actions
  end
  
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name,
                :alias_name,
                :sex,
                :nation,
                :birthday,
                :deathday,
                :birthplace,
                :occupation,
                :biographical_text,
                :avatar
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :alias_name, :sex, :nation, :birthday, :deathday, :birthplace, :occupation, :biographical_text]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
