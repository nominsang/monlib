ActiveAdmin.register Document do
  menu priority: 4, label: "古籍"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  
  index do
    selectable_column
    id_column
    column :mongolian_title
    column :chinese_title
    actions
  end
  
  show do
    attributes_table do
      row :doc_no
      row :mongolian_title do
        resource.mongolian_title.gsub(/=/, '<br>').html_safe
      end
      row :m_title
      row :chinese_title
      row :latin_title
      row :title_source
      row :creator
      row :publisher
      row :release_time_id do
        ReleaseTime.find(resource.release_time_id).title
      end
      row :release_time_plus
      row :publish_tech_id do
        PublishTech.find(resource.publish_tech_id).title
      end
      row :shape_id do
        Shape.find(resource.shape_id).title
      end
      row  :material_id do
        Material.find(resource.material_id).title
      end
      row  :color_id do
        Color.find(resource.color_id).title
      end
      row  :size
      row  :is_translation
      row  :is_complete
      row  :has_preamble
      row  :has_postscript
      row  :remark
      row  :category_id do
        c = Category.find(resource.category_id)
        c.slug + ": " + c.title + " / " + c.about
      end
      row  :notes
      row  :collector
      row  :volumeset
      row  :language_id do
        Language.find(resource.language_id).title
      end
    end
  end
  
  sidebar "馆藏信息", only: [:show, :edit] do
    ul do
      li link_to "所有馆藏信息", admin_document_items_path(resource)
      li link_to "新增馆藏信息", new_admin_document_item_path(resource)
    end
  end
  
  permit_params :doc_no,
                :mongolian_title, 
                :m_title, 
                :chinese_title, 
                :latin_title, 
                :title_source, 
                :creator, 
                :publisher, 
                :release_time_id, 
                :release_time_plus, 
                :publish_tech_id, 
                :shape_id, 
                :material_id, 
                :color_id, 
                :size, 
                :is_translation, 
                :is_complete, 
                :has_preamble, 
                :has_postscript, 
                :remark, 
                :category_id, 
                :notes, 
                :collector, 
                :volumeset, 
                :language_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:doc_no, :mongolian_title, :m_title, :chinese_title, :latin_title, :title_source, :creator, :publisher, :release_time_id, :release_time_plus, :publish_tech_id, :shape_id, :material_id, :color_id, :size, :is_translation, :is_complete, :has_preamble, :has_postscript, :remark, :category_id, :notes, :collector, :volumeset, :language_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
