ActiveAdmin.register Paper do
  belongs_to :issue

  breadcrumb do
    ise = Issue.find(params[:issue_id])
    journal_path = "/admin/journals/" + ise.journal.id.to_s
    issues_path = journal_path + "/issues"
    issue_path = issues_path + "/" + ise.id.to_s
    ["<a href='/admin'>首页</a>".html_safe,
     "<a href='/admin/journals/'>期刊</a>".html_safe,
     "<a href=#{journal_path}>#{ise.journal.title}</a>".html_safe,
     "<a href=#{issues_path}>卷期</a>".html_safe,
     "<a href=#{issue_path}>#{ise.display_name}</a>".html_safe,
     ]
  end
  
  index do
    selectable_column
    column :title
    column :author
    column :keywords
    column :issue
    actions
  end
  #menu parent: "期刊", priority: 2

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  
  form do |f|
    f.inputs do
      f.input :issue
      f.input :title
      f.input :chinese_title
      f.input :english_title
      f.input :author
      f.input :chinese_author
      f.input :english_author
      f.input :unit
      f.input :chinese_unit
      f.input :english_unit
      f.input :keywords
      f.input :chinese_keywords
      f.input :english_keywords
      f.input :category
      f.input :page_from
      f.input :page_to
      f.input :abstract
      f.input :article, as: :file
      f.input :finish
    end
    actions
  end
  
  show do
    attributes_table do
      row :issue
      row :title
      row :chinese_title
      row :english_title
      row :author
      row :chinese_author
      row :english_author
      row :unit
      row :chinese_unit
      row :english_unit
      row :keywords
      row :chinese_keywords
      row :english_keywords
      row :category
      row :page_from
      row :page_to
      row :abstract
      row :article do
        div do
          if paper.article.attached?
            paper.article.filename
          else
            "no"
          end
        end
      end
      row :finish
    end
  end
  
  permit_params :issue_id,
                :title,
                :chinese_title,
                :english_title,
                :author,
                :chinese_author,
                :english_author,
                :unit,
                :chinese_unit,
                :english_unit,
                :keywords,
                :chinese_keywords,
                :english_keywords,
                :category,
                :page_from,
                :page_to,
                :abstract,
                :finish,
                :article
  #
  # or
  #
  # permit_params do
  #   permitted = [:issue_id, :title, :chinese_title, :english_title, :author, :chinese_author, :english_author, :unit, :chinese_unit, :english_unit, :keywords, :chinese_keywords, :english_keywords, :category, :page_from, :page_to, :abstract, :finish]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  

end
