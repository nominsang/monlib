ActiveAdmin.register Book do
  menu priority: 2
  sidebar "目录", only: [:show, :edit] do
    ul do
      li link_to "所有目录条目", admin_book_book_tocs_path(resource)
      li link_to "新增目录条目", new_admin_book_book_toc_path(resource)
    end
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :title, :alternative_title, :creator, :contributor, :isbn, :cadal_id, :publish_date, :publisher, :subject, :language, :total_file, :price, :abstract
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :alternative_title, :creator, :contributor, :isbn, :cadal_id, :publish_date, :publisher, :subject, :language, :total_file, :price, :abstract]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
