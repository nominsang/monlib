ActiveAdmin.register Issue do
  belongs_to :journal
  sidebar "该卷期文章", only: [:show, :edit] do
    ul do
      li link_to "所有文章", admin_issue_papers_path(resource)
      li link_to "新增文章", new_admin_issue_paper_path(resource)
    end
  end
  
  form do |f|
    f.inputs do
      f.input :journal
      f.input :volume
      f.input :issue
      f.input :year
      f.input :facebook, as: :file
      actions
    end
  end
  
  show do
    attributes_table do
      row :journal
      row :volume
      row :issue
      row :year
      row :facebook do
        div do
          if issue.facebook.attached?
            issue.facebook.filename
          else
            "no"
          end
        end
      end
    end
  end
  #menu parent: "期刊", priority: 1
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :journal_id, :volume, :issue, :year, :facebook
  #
  # or
  #
  # permit_params do
  #   permitted = [:journal_id, :volume, :issue, :year]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
