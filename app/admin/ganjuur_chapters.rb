ActiveAdmin.register GanjuurChapter do
  menu priority: 5, label: "甘珠尔"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :doc_no, :folder, :title, :title_mongolian, :title_latin, :title_zhs, :title_zht, :original_id, :pages, :start_page
  #
  # or
  #
  # permit_params do
  #   permitted = [:doc_no, :folder, :title, :title_mongolian, :title_latin, :title_zhs, :title_zht, :original_id, :pages, :start_page]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
