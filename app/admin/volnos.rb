ActiveAdmin.register Volno do
  belongs_to :item
  
  sidebar "登录号/财产号", only: [:show, :edit] do
    ul do
      li link_to "所有登录号", admin_volno_entities_path(resource)
      li link_to "新增登录号", new_admin_volno_entity_path(resource)
    end
  end
  
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :item_id, :volno_code, :about
  #
  # or
  #
  # permit_params do
  #   permitted = [:item_id, :volno_code, :about]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
