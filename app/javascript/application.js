// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

if (navigator.userAgent.indexOf("Firefox") !== -1) {
    // Do something if the browser is Firefox
} else {
    window.location.href = '/firefox';
}
