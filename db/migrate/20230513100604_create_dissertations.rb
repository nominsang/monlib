class CreateDissertations < ActiveRecord::Migration[7.0]
  def change
    create_table :dissertations do |t|
      t.string :title
      t.string :author
      t.string :teacher
      t.string :keywords
      t.text :abstract
      t.integer :student_id
      t.string :discipline
      t.string :research_field
      t.string :education
      t.string :degree
      t.integer :graduation_date

      t.timestamps
    end
  end
end
