class CreateBookTocs < ActiveRecord::Migration[7.0]
  def change
    create_table :book_tocs do |t|
      t.references :book, null: false, foreign_key: true
      t.string :chapter
      t.string :title
      t.string :page
      t.string :file
      t.integer :order

      t.timestamps
    end
  end
end
