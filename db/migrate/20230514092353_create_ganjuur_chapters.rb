class CreateGanjuurChapters < ActiveRecord::Migration[7.0]
  def change
    create_table :ganjuur_chapters do |t|
      t.string :doc_no
      t.integer :folder
      t.string :title
      t.string :title_mongolian
      t.string :title_latin
      t.string :title_zhs
      t.string :title_zht
      t.string :original_id
      t.integer :pages
      t.string :start_page

      t.timestamps
    end
  end
end
