class CreateBooks < ActiveRecord::Migration[7.0]
  def change
    create_table :books do |t|
      t.string :title
      t.string :alternative_title
      t.string :creator
      t.string :contributor
      t.string :isbn
      t.string :cadal_id
      t.string :publish_date
      t.string :publisher
      t.string :subject
      t.string :language
      t.string :total_file
      t.string :price
      t.text :abstract

      t.timestamps
    end
  end
end
