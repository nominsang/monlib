# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_05_15_020750) do
  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.integer "resource_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "book_tocs", force: :cascade do |t|
    t.integer "book_id", null: false
    t.string "chapter"
    t.string "title"
    t.string "page"
    t.string "file"
    t.integer "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id"], name: "index_book_tocs_on_book_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "title"
    t.string "alternative_title"
    t.string "creator"
    t.string "contributor"
    t.string "isbn"
    t.string "cadal_id"
    t.string "publish_date"
    t.string "publisher"
    t.string "subject"
    t.string "language"
    t.string "total_file"
    t.string "price"
    t.text "abstract"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dissertations", force: :cascade do |t|
    t.string "title"
    t.string "author"
    t.string "teacher"
    t.string "keywords"
    t.text "abstract"
    t.integer "student_id"
    t.string "discipline"
    t.string "research_field"
    t.string "education"
    t.string "degree"
    t.integer "graduation_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "experts", force: :cascade do |t|
    t.string "name"
    t.string "alias_name"
    t.string "sex"
    t.string "nation"
    t.string "birthday"
    t.string "deathday"
    t.string "birthplace"
    t.string "occupation"
    t.text "biographical_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ganjuur_chapters", force: :cascade do |t|
    t.string "doc_no"
    t.integer "folder"
    t.string "title"
    t.string "title_mongolian"
    t.string "title_latin"
    t.string "title_zhs"
    t.string "title_zht"
    t.string "original_id"
    t.integer "pages"
    t.string "start_page"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "issues", force: :cascade do |t|
    t.integer "journal_id", null: false
    t.integer "volume"
    t.integer "issue"
    t.integer "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["journal_id"], name: "index_issues_on_journal_id"
  end

  create_table "journals", force: :cascade do |t|
    t.string "title"
    t.string "chinese_title"
    t.string "english_title"
    t.string "slug"
    t.string "name_ever"
    t.string "since"
    t.string "issn"
    t.string "sponsor"
    t.text "introduce"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mon_experts", force: :cascade do |t|
    t.integer "expert_id", null: false
    t.string "name"
    t.string "alias_name"
    t.string "nation"
    t.string "birthplace"
    t.string "occupation"
    t.text "biographical_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["expert_id"], name: "index_mon_experts_on_expert_id"
  end

  create_table "papers", force: :cascade do |t|
    t.integer "issue_id", null: false
    t.string "title"
    t.string "chinese_title"
    t.string "english_title"
    t.string "author"
    t.string "chinese_author"
    t.string "english_author"
    t.string "unit"
    t.string "chinese_unit"
    t.string "english_unit"
    t.string "keywords"
    t.string "chinese_keywords"
    t.string "english_keywords"
    t.string "category"
    t.integer "page_from"
    t.integer "page_to"
    t.text "abstract"
    t.boolean "finish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["issue_id"], name: "index_papers_on_issue_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "book_tocs", "books"
  add_foreign_key "issues", "journals"
  add_foreign_key "mon_experts", "experts"
  add_foreign_key "papers", "issues"
end
